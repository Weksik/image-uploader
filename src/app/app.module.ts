/*====Core====*/
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

/*====Material====*/
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

/*====User Components and Modules====*/
import { UploaderModule } from './uploader/uploader.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    UploaderModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
