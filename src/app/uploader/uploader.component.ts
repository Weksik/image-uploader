import { Component, Input, OnInit, Inject } from '@angular/core';
import { UploaderService } from './uploader.service';
import { MatDialog } from '@angular/material/dialog';
import { FileExplorerComponent } from './file-explorer/file-explorer.component';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss'],
  providers: [UploaderService],
})
export class UploaderComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  @Input() buttonOpenExplorer: string = 'Open File Explorer';

  openDialog(): void {
    const dialogRef = this.dialog.open(FileExplorerComponent, {
      width: '80%',
      height: '80%',
      panelClass: 'file-explorer-dialog',
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log('The dialog was closed');
      console.log(result);
    });
  }

  ngOnInit(): void {}
}
