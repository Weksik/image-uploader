import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UploaderComponent } from './uploader.component';

import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { FileExplorerComponent } from './file-explorer/file-explorer.component';

const ANGULAR_MATERIAL = [MatButtonModule, MatDialogModule];

@NgModule({
  declarations: [UploaderComponent, FileExplorerComponent],
  imports: [CommonModule, ...ANGULAR_MATERIAL],
  exports: [UploaderComponent],
})
export class UploaderModule {}
